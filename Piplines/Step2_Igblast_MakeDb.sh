#!/bin/bash
# Gets as an input presto out file 
#check commit2
while getopts ":p:" opt; do             ###getopts NOT getopt! 
  case $opt in
  p)p=${OPTARG};;
   esac
done
shift $(( OPTIND - 1 ))

FILEPARAM=$p
FILES=$@

echo "p is "$p""
echo "FILES are  "$@""
echo "FILES are  "$FILES""
#FILEPARAM=$p
#FILES=$f

FILEDIR=`basename "$PWD"`
FILEPATH=$PWD

echo $FILES

. "$FILEPARAM"

cat "$FILEPARAM" > $PARAMLOG   ##check this
cd $IGBLAST_DIR


for FILE in ${FILES}
do

NPROC=$(($NPROC+1))
    if [ "$NPROC" -ge 4 ]; then
        wait
        NPROC=0
    fi

FILE_NAME=`echo ${FILE%%.*}`
printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 24 "Igblast"
echo "STARTED IgBLAST "$FILE
$RUN ./igblastn -germline_db_V $GERM_DB_V -germline_db_J $GERM_DB_J -germline_db_D $GERM_DB_D -organism $ORGANISM -domain_system imgt -query "$FILEPATH""/"$FILE -auxiliary_data $AUX_DATA -show_translation -outfmt "$OUT_FORMAT" -num_threads $N_TREADS -ig_seqtype $CELL_TYPE > "$FILEPATH""/"$FILE_NAME"_igblast.log" &

done
wait
 
echo "FINISHED IgBLAST ON ALL FILES"

cd "$FILEPATH"
wait



#MakeDb



for FILE in ${FILES}
do
echo $FILE
FILE_NAME=`echo ${FILE%%.*}`
echo $FILE_NAME
# Required Arguments:
#   $1 = IgBLAST output file
#   $2 = FASTA file that was submitted to IMGT
#   $3 = the folder(s) or file(s) containing the IMGT reference database sequences
#   $4 = output directory
#   $5 = output file prefix
#   $6 = number of subprocesses for multiprocessing tools


# Capture command line parameters
IGBLAST_FILE=$(readlink -f  $FILE_NAME"_igblast.log")
SEQ_FILE=$(readlink -f $FILE)
GERM_DIR=$(readlink -f $REPO)
OUTDIR=$(readlink -f $FILEPATH)


OUTNAME=$FILE_NAME"_MakeDb_Table"
NPROC=4



# DefineClones parameters
DC_MODEL=hs1f
DC_DIST=0
DC_ACT=first
DC_SFIELD=JUNCTION

# Create germlines parameters
CG_GERM=dmask
CG_SFIELD=SEQUENCE_IMGT
CG_VFIELD=V_CALL

# Define log files
PIPELINE_LOG=$FILE_NAME"_Pipeline.log"
ERROR_LOG=$FILE_NAME"_Pipeline.err"

# Make output directory and empty log files
mkdir -p $OUTDIR; cd $OUTDIR
echo '' > $PIPELINE_LOG
echo '' > $ERROR_LOG


# Start

echo -e "\nSTART"

echo "$IGBLAST_FILE"
STEP=0

# Parse IgBLAST output
printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 24 "MakeDb igblast"
MakeDb.py igblast -i $IGBLAST_FILE -s $SEQ_FILE --outname "${OUTNAME}" \
        -r $GERM_DIR --outdir . >> $PIPELINE_LOG 2> $ERROR_LOG
# End
done
echo -e "DONE\n"
wait

