rm(list=ls())

library(ggplot2)
library(grid)
library(hexbin)
library(plyr)
library(reshape2)
library(scales)
library(stringr)
library(gplots)
library(gsubfn)
library(proto)
library(RSQLite)
library(DBI)
library(sqldf)
library(tcltk)
#library(RnavGraph)

library(stats)
library(base)
library(alakazam)
library(doSNOW)
library(foreach)
library(iterators)
library(snow)
library(dplyr)
library(tigger)
library(ggplot2)
library(plyr)
library(shm)
library(gsubfn)
library(proto)
library(RSQLite)
library(DBI)
library(seqinr)
library(latticeExtra)

closeAllConnections(); graphics.off()


########################sources and setwd CHANGES##############################################################
##############################FROM HOME####################
setwd('C:/Users/DELL/Documents/PHD')####****change  here                               # 
source('C:/Users/DELL/Dropbox (Engineering Bar Ilan)/hcvcodes/CodesForGraphs/DataCore.R')####****change  here                  #
source('C:/Users/DELL/Dropbox (Engineering Bar Ilan)/hcvcodes/CodesForGraphs/AnalysisAfterIGBLAST/New_functions_for_Plots.R')####****change  here
source('C:/Users/DELL/Dropbox (Engineering Bar Ilan)/hcvcodes/STEP2/Tigger/tigger/R/data.r')####****change  here                  #
source('C:/Users/DELL/Dropbox (Engineering Bar Ilan)/hcvcodes/STEP2/Tigger/tigger/R/functions.r')####****change  here   # 
parent.folder<-"C:/Users/DELL/Dropbox (Engineering Bar Ilan)/HCVDataSamples/504"


############################FROM LAB###########################
#setwd('/media/Data/Dropbox/HCVCodes/STEP2/RESULTS')####****change  here                               # 
#source('/media/Data/Dropbox/HCVCodes/CodesForGraphs/DataCore.R')####****change  here                  #
#source('/media/Data/Dropbox/HCVCodes/CodesForGraphs/AnalysisAfterIGBLAST/New_functions_for_Plots.R')####****change  here
#source('/media/Data/Dropbox/HCVCodes/STEP2/Tigger/tigger/R/data.R')####****change  here                  #
#source('/media/Data/Dropbox/HCVCodes/STEP2/Tigger/tigger/R/functions.R')####****change  here   # 
#parent.folder<-"/media/Data/Dropbox/HCVDataSamples/430"

##############################FROM HOME####################
#setwd('C:/Users/ebernstx/Documents/PHD')####****change  here                               # 
#source('C:/Users/ebernstx/Dropbox (Engineering Bar Ilan)/hcvcodes/CodesForGraphs/DataCore.R')####****change  here                  #
#source('C:/Users/ebernstx/Dropbox (Engineering Bar Ilan)/hcvcodes/CodesForGraphs/AnalysisAfterIGBLAST/New_functions_for_Plots.R')####****change  here
#source('C:/Users/ebernstx/Dropbox (Engineering Bar Ilan)/hcvcodes/STEP2/Tigger/tigger/R/data.r')####****change  here                  #
#source('C:/Users/ebernstx/Dropbox (Engineering Bar Ilan)/hcvcodes/STEP2/Tigger/tigger/R/functions.r')####****change  here   # 
#parent.folder<-"C:/Users/ebernstx/Dropbox (Engineering Bar Ilan)/HCVDataSamples/TEST"


####################

ncol=3 #######***no. col change here (need to update the code) 
FalseTrueSign<-rbind('TRUE','FALSE')#
NonFunctReasoncolours <- c("OpenFrame"="#984EA3", "StopCodon"="#9999FF","Indels"="#D92121","Other"="#21D921")
FuncOrNotcolours <- c('TRUE'="#377EB8",'FALSE'="burlywood1")

#############################################################################################
font=8
################FROM HOME
germline_ighvrepo<-Read_fasta('C:/Users/DELL/Dropbox (Engineering Bar Ilan)/hcvcodes/REPO/REPO.fasta')

##### FOR LAB ###########################
#change also in function findNovellallels in TIGger
#germline_ighvrepo<-Read_fasta('/media/Data/Dropbox/HCVCodes/REPO/REPO.fasta')

################FROM HOME
#germline_ighvrepo<-Read_fasta('C:/Users/ebernstx/Dropbox (Engineering Bar Ilan)/hcvcodes/REPO/REPO.fasta')
##### FOR LAB ###########################
#change also in function findNovellallels in TIGger
#germline_ighvrepo<-Read_fasta('/media/Data/Dropbox/HCVCodes/REPO/REPO.fasta')

################FROM HOME
#germline_ighvrepo<-Read_fasta('C:/Users/ebernstx/Dropbox (Engineering Bar Ilan)/hcvcodes/REPO/REPO.fasta')


#sub.folders<-list.dirs(parent.folder,recursive=TRUE)[-1]
sub.folders<-list.dirs(parent.folder,recursive=FALSE)

FolderNames<-list()
ReasonSummary<-list()
FunOrNot<-list()
DupcountTable<-list()
JunLenDistAllSamples<-list()
VJDistAll<-list()
VDIST<-list()
V_call_all<-list()
VDistAllSamples<-list()
p<-list()
VdistPlot<-list()
VDJ_DistAll<-list()
GroupTitle="Samples: "
p3D<-list()
i=0

########################################START LOOP####################################
for (fold in sub.folders){ 
  PathSplit<-strsplit(fold,"/",fixed=TRUE)[[1]]
  FolderNames[[i+1]]=PathSplit[[length(PathSplit)]]
  GroupTitle=paste0(GroupTitle,FolderNames[[i+1]],", ")
  print(FolderNames[[i+1]])
  sample_path=fold
  DataAfterImgt<-loadLogTable(file.path(sample_path, "Assembled_Collapsed_IG_MakeDb_Table_db-pass.tab")) #name of table
  
  ## split PRCONS and get only IG part
  
  NoCol=ncol(DataAfterImgt)
  DataAfterImgt[,NoCol+1] <- sapply(strsplit(as.character( DataAfterImgt$PRCONS), fixed(",")), "[",1)
  colnames(DataAfterImgt)[colnames(DataAfterImgt)=='PRCONS'] <- 'PRCONS_FULL'
  names(DataAfterImgt)[1+NoCol]<-'PRCONS'
  
  
  ## Recollapse based on VDJ_seq and PRCONS
  
  DataAfterImgt<-sqldf("select *,sum(DUPCOUNT) as DUPCOUNT_TOTAL, sum(CONSCOUNT) as CONSCOUNT_TOTAL  from DataAfterImgt GROUP BY  SEQUENCE_VDJ, PRCONS, FUNCTIONAL")
  colnames(DataAfterImgt)[colnames(DataAfterImgt)=='DUPCOUNT'] <- 'DUPCOUNT_NOT_USE1'
  colnames(DataAfterImgt)[colnames(DataAfterImgt)=='CONSCOUNT'] <- 'CONSCOUNT_NOT_USE2'
  colnames(DataAfterImgt)[colnames(DataAfterImgt)=='DUPCOUNT_TOTAL'] <- 'DUPCOUNT'
  colnames(DataAfterImgt)[colnames(DataAfterImgt)=='CONSCOUNT_TOTAL'] <- 'CONSCOUNT'
  
  
  ## Functional and not analysis
  
  IgData1<-FunctionalAnalysis(DataAfterImgt, FolderNames[[i+1]],FalseTrueSign)
  ReasonSummary<-rbind(ReasonSummary,IgData1[[2]])
  FunOrNot<-rbind(FunOrNot,IgData1[[1]])
  
  i=i+1
  
}

#########################Graphs####################################
###############Graphs functional or not
TotalRows <- ddply(FunOrNot, .(Sample), summarize, 
                   TotalFT=sum(Count))
RR<-merge(FunOrNot,TotalRows,by="Sample")
p[[1]]<-ggplot(ReasonSummary, aes(x=factor(Sample),y=Count,fill=factor(Reason)))+
  geom_bar(width = 0.8, stat = "identity")+
  xlab("") +
  ylab("") +
  theme( axis.text.x=element_text(angle=90))+
  scale_fill_manual(values = NonFunctReasoncolours,name="Reasons")

p[[2]]<-ggplot(RR, aes(x=factor(Sample),y=Count/TotalFT,fill=factor(FUNCTIONAL)))+
  geom_bar(width = 0.8, stat = "identity")+
  xlab("") +
  ylab("") +
  scale_y_continuous(labels=percent)+
  theme( axis.text.x=element_text(angle=90))+
  scale_fill_manual(values = FuncOrNotcolours,name="FUNCTIONAL")

p[[3]]<-ggplot(FunOrNot, aes(x=factor(Sample),y=Count,fill=factor(FUNCTIONAL)))+
  geom_bar(width = 0.8, stat = "identity")+
  xlab("") +
  ylab("") +
  
  theme( axis.text.x=element_text(angle=90))+
  scale_fill_manual(values = FuncOrNotcolours,name="FUNCTIONAL")

ncol1=1
nrow=2
PdfName=paste0("430_Graphs.pdf")
pdf(PdfName) #,width=2*ncol1, height=5*nrow)

Newmultiplot(p) 


dev.off()


